<?php

/**
 * @file
 * EPUB Reader Framework API documentation.
 */

use \Drupal\Core\Entity\ContentEntityInterface;
use \Drupal\node\Entity\Node;
use \Drupal\node\NodeInterface;

/**
 * Perform alterations on the current node used to determine previous and next.
 *
 * @param \Drupal\node\NodeInterface $node
 *   The current node used to determine previous and next elements.
 *
 * @throws \Drupal\Core\TypedData\Exception\MissingDataException
 */
function hook_epub_reader_framework_current_node_alter(NodeInterface &$node) {
  if ($node->bundle() == 'reader_publication') {
    if ($node->get('show_first_chapter_on_landing')->value) {
      return;
    }

    if (!$node->get('field_reader_chapters')->count()) {
      return;
    }

    $first_chapter_reference = $node->get('field_reader_chapters')->first();
    if ($first_chapter_reference && isset($first_chapter_reference->entity)) {
      $node = $first_chapter_reference->entity;
    }
  }
}

/**
 * Perform alterations on the label for the chapter label in the nav block.
 *
 * @param string $chapter_label
 *   The reader chapter label.
 * @param \Drupal\node\NodeInterface $chapter
 *   The reader chapter node id.
 */
function hook_epub_reader_framework_navigation_chapter_label_alter($chapter_label, NodeInterface $chapter) {
  if ($chapter) {
    $chapter_label = t('Chapter') . ' ' . $chapter->label();
  }
}

/**
 * Perform alterations on the label for the chapter heading in the nav block.
 *
 * This can result in any render array.
 *
 * @param \Drupal\Core\Entity\ContentEntityInterface $reader_chapter_heading
 *   The reader chapter heading entity.
 * @param int $reader_chapter_id
 *   The reader chapter node id.
 * @param int $count
 *   The current nested heading number. Eg, 2 for the second heading within this chapter.
 */
function hook_epub_reader_framework_navigation_chapter_heading_alter(ContentEntityInterface &$reader_chapter_heading, $reader_chapter_id, $count) {
  if ($reader_chapter = Node::load($reader_chapter_id)) {
    /** @var NodeInterface $reader_chapter_heading */
    $reader_chapter_heading = [
      '#plain_text' => t('Chapter') . $count . ': ' . $reader_chapter->label(),
    ];
  }
}

/**
 * Perform alterations on the chapter link item markup.
 *
 * @param array $chapter_menu_item
 *   The chapter menu item including link and icon.
 */
function hook_epub_reader_framework_navigation_chapter_menu_item_alter(&$chapter_menu_item) {
  $chapter_menu_item['#attributes'] = array_merge_recursive([
    'class' => ['u-container'],
  ]);
}

/**
 * Perform alterations on the icon expanding to show the nested chapter headings in the navigation block.
 *
 * This can result in any render array.
 *
 * @param array $icon
 *   The expand icon render array.
 */
function hook_epub_reader_framework_navigation_expand_icon_alter(array &$icon) {
  $icon = [
    '#plain_text' => t('Click to expand'),
  ];
}

/**
 * Change the navigation block ID to auto-load on the reader publication and chapter pages.
 *
 * Set the block ID to an empty string to skip auto-loading.
 *
 * @param string $block_id
 *   The block ID to load.
 */
function hook_epub_reader_framework_navigation_block_id_alter(&$block_id) {
  $block_id = 'custom_implementation_of_reader_navigation_block';
}

/**
 * Change the previous next block ID to auto-load on the reader publication and chapter pages.
 *
 * Set the block ID to an empty string to skip auto-loading.
 *
 * @param string $block_id
 *   The block ID to load.
 */
function hook_epub_reader_framework_previous_next_block_id_alter(&$block_id) {
  $block_id = 'custom_implementation_of_reader_previous_next_block';
}
