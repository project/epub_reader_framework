<?php

namespace Drupal\Tests\epub_reader_framework\FunctionalJavascript;

use Drupal\Tests\BrowserTestBase;

/**
 * Class ReaderUploadEpubTest.
 *
 * @group epub_reader_framework
 */
class ReaderUploadEpubTest extends BrowserTestBase {

  /**
   * Default theme to test on. We rely on css classes.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Installation profile to use.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Allow up to 10 batch process redirects.
   *
   * @var int
   */
  protected $maximumMetaRefreshCount = 10;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'field_group',
    'epub_reader_framework',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $admin_user = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests the add widget button with modal form.
   */
  public function testCreateReaderPublicationAndSeeChapters() {

    // Make sure there is an EPUB media type to add.
    $this->drupalGet('media/add/epub');

    // Add an EPUB file.
    $filepath = drupal_get_path('module', 'epub_reader_framework');
    $filepath .= '/tests/fixtures/epubs/trees.epub';
    $this->getSession()->getPage()->attachFileToField('files[field_media_file_0]', $filepath);
    $this->drupalPostForm(NULL, [], t('Save'));

    // Create the reader publication.
    $this->drupalGet('node/add/reader_publication');
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Test EPUB',
      'field_reader_file[0][target_id]' => 'trees.epub (1)',
    ], t('Save'));

    // Wait for batch completion.
    $this->checkForMetaRefresh();

    // Check that the chapters were successfully extracted.
    $this->assertSession()->pageTextContains('Reader publication Test EPUB has been created.');
    $this->assertSession()->pageTextContains('3 chapters were extracted successfully.');

    // Check that the chapter links exist on the page.
    $this->assertSession()->linkExists('ttl');
    $this->assertSession()->linkExists('c1');
    $this->assertSession()->linkExists('c2');
  }

}
