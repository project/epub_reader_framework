<?php

namespace Drupal\Tests\epub_reader_framework\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class ReaderPreviousNextTest.
 *
 * @group epub_reader_framework
 */
class ReaderPreviousNextTest extends BrowserTestBase {

  /**
   * Default theme to test on. We rely on css classes.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Installation profile to use.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'field_group',
    'epub_reader_framework',
    'epub_reader_framework_implementation_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $admin_user = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests the add widget button with modal form.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testChapterNavigation() {

    // Create the reader publication.
    $this->drupalGet('node/add/reader_publication');
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Test EPUB',
    ], t('Save'));

    // Go to the add chapter via publication edit.
    $this->drupalGet('node/1/edit');
    $this->clickLink(t('Add Chapter'));

    // Ensure we are on the chapter page.
    $this->assertSession()->pageTextContains('Create Reader chapter');

    // Create a chapter.
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Chapter One',
    ], t('Save'));

    // We should have been redirected to the publication.
    $this->clickLink(t('Add Chapter'));

    // Create a chapter.
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Chapter Two',
    ], t('Save'));

    // Ensure we have the two chapters on the page.
    $this->assertSession()->pageTextContains('Chapter One');
    $this->assertSession()->pageTextContains('Chapter Two');

    // View the test EPUB.
    $this->drupalGet('/node/1');

    // Click on the start reading.
    $this->clickLink(t('Start reading'));
    $this->checkForMetaRefresh();
    $this->assertSession()->elementContains('css', 'article h2', 'Chapter One');

    // Click on the next chapter button.
    $this->clickLink(t('Next chapter'));
    $this->checkForMetaRefresh();
    $this->assertSession()->elementContains('css', 'article h2', 'Chapter Two');

    // Should be the last chapter.
    $this->assertSession()->elementNotContains('css', '.node__previous-next', 'Next chapter');

    // Back to chapter one.
    $this->clickLink(t('Previous chapter'));
    $this->checkForMetaRefresh();
    $this->assertSession()->elementContains('css', 'article h2', 'Chapter One');
  }

}
