<?php

namespace Drupal\Tests\epub_reader_framework\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class ReaderChapterNavigationTest.
 *
 * @group epub_reader_framework
 */
class ReaderChapterNavigationTest extends BrowserTestBase {

  /**
   * Default theme to test on. We rely on css classes.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Installation profile to use.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'field_group',
    'epub_reader_framework',
    'epub_reader_framework_implementation_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $admin_user = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests the add widget button with modal form.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testChapterNavigation() {

    // Create the reader publication.
    $this->drupalGet('node/add/reader_publication');
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Test EPUB',
    ], t('Save'));

    // Go to the add chapter via publication edit.
    $this->drupalGet('node/1/edit');
    $this->clickLink(t('Add Chapter'));

    // Ensure we are on the chapter page.
    $this->assertSession()->pageTextContains('Create Reader chapter');

    // Create a chapter.
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Chapter One',
    ], t('Save'));

    // We should have been redirected to the publication.
    $this->clickLink(t('Add Chapter'));

    // Create a chapter.
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Chapter Two',
    ], t('Save'));

    // Ensure we have the two chapters on the page.
    $this->assertSession()->pageTextContains('Chapter One');
    $this->assertSession()->pageTextContains('Chapter Two');

    // Add the chapter navigation block.
    $this->drupalPlaceBlock('reader_navigation_block', [
      'label' => t('Reader navigation block'),
    ]);

    // View the test EPUB.
    $this->drupalGet('/node/1');

    // Ensure the block is appearing.
    $this->assertSession()->pageTextContains('Reader navigation block');

    // Ensure the block has two chapters.
    $this->assertSession()->pageTextContains('Chapter One');
    $this->assertSession()->pageTextContains('Chapter Two');
  }

}
