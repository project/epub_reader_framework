<?php

namespace Drupal\Tests\epub_reader_framework\Functional;

use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;

/**
 * Class ReaderSubchapterHeadingsTest.
 *
 * @group epub_reader_framework
 */
class ReaderSubchapterHeadingsTest extends BrowserTestBase {

  /**
   * Default theme to test on. We rely on css classes.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Installation profile to use.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'field_group',
    'epub_reader_framework',
    'epub_reader_framework_implementation_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $admin_user = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests the add widget button with modal form.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testChapterSubheadings() {

    // Add the body field to a chapter.
    $reader_chapter_type = NodeType::load('reader_chapter');
    node_add_body_field($reader_chapter_type);

    // Create the reader publication.
    $this->drupalGet('node/add/reader_publication');
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Test EPUB',
    ], t('Save'));

    // Go to the add chapter via publication edit.
    $this->drupalGet('node/1/edit');
    $this->clickLink(t('Add Chapter'));

    // Create a chapter.
    $this->drupalPostForm(NULL, [
      'title[0][value]' => 'Chapter One',
      'body[0][value]' => '
        <h2>Chapter One Subheading One</h2>
        <p>Some content</p>
        <h3>Level 3 here</h3>
        <p>More content</p>
        <h2>Chapter One Subheading Two</h2>
        <p>Even more content</p>
      ',
    ], t('Save'));

    // Add the chapter navigation block.
    $this->drupalPlaceBlock('reader_navigation_block', [
      'label' => t('Reader navigation block'),
    ]);

    // View the test EPUB.
    $this->drupalGet('/node/1');

    // Ensure the block is appearing.
    $this->assertSession()->pageTextContains('Reader navigation block');

    // Ensure the block sub headings are appearing.
    $this->assertSession()->pageTextContains('Chapter One Subheading One');
    $this->assertSession()->pageTextContains('Chapter One Subheading Two');

    // Ensure deeper headings are not appearing.
    $this->assertSession()->pageTextNotContains('Level 3 here');
  }

}
