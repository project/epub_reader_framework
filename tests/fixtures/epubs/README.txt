FILE CREDITS
------------

The EPUBs are from the "EPUB 3 Samples Project" maintained
by W3C's EPUB 3 Community Group.

https://idpf.github.io/epub3-samples/
