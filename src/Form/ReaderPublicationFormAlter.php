<?php

namespace Drupal\epub_reader_framework\Form;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Access\NodeAddAccessCheck;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class ReaderPublicationFormAlter.
 */
class ReaderPublicationFormAlter {

  use StringTranslationTrait;

  /**
   * The access manager service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The node access service.
   *
   * @var \Drupal\node\Access\NodeAddAccessCheck
   */
  protected $nodeAddAccessCheck;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ReaderPublicationFormAlter object.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The route match service.
   * @param \Drupal\node\Access\NodeAddAccessCheck $node_access
   *   The node access check.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    CurrentRouteMatch $route_match,
    NodeAddAccessCheck $node_access,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->routeMatch = $route_match;
    $this->nodeAddAccessCheck = $node_access;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('access_check.node.add'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Callback implementation of hook_form_FORM_ID_alter().
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $form_id
   *   The form ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function formAlter(array &$form, FormStateInterface $form_state, $form_id) {

    // If we are on the publication reader page.
    if (isset($form['field_reader_chapters']['widget']['add_more'])) {

      // If we are on the node add page, remove chapters completely.
      if ($this->routeMatch->getRouteName() == 'node.add') {
        $form['field_reader_chapters']['#access'] = FALSE;
      }

      // Remove normal add more button.
      $form['field_reader_chapters']['widget']['add_more']['#access'] = FALSE;

      // Update the chapter items to be non-editable.
      foreach ($form['field_reader_chapters']['widget'] as $key => &$item) {
        if (is_numeric($key)) {

          // Don't allow the add new item.
          if (!$item['target_id']['#default_value']) {
            unset($form['field_reader_chapters']['widget'][$key]);
          }
          elseif ($item['target_id']['#default_value'] instanceof NodeInterface) {
            /** @var \Drupal\node\NodeInterface $target_node */
            $target_node = $item['target_id']['#default_value'];
            $item['target_id']['#access'] = FALSE;

            // Add a link to the edit node screen.
            $url = Url::fromRoute('entity.node.edit_form', [
              'node' => $target_node->id(),
            ]);
            $link = Link::fromTextAndUrl($target_node->label(), $url);
            $item['#suffix'] = $link->toString();
          }
        }
      }

      // Add an "Add new chapter" button.
      /** @var \Drupal\Core\Session\AccountProxyInterface $user */
      $user = $this->currentUser;
      /** @var \Drupal\node\Access\NodeAddAccessCheck $access_check */
      $access_check = $this->nodeAddAccessCheck;
      $reader_chapter_type = $this->entityTypeManager
        ->getStorage('node_type')
        ->load('reader_chapter');
      if ($access_check->access($user, $reader_chapter_type)) {
        $url = Url::fromRoute('node.add', ['node_type' => 'reader_chapter']);
        if ($url->access($user)) {

          // If we have a node ID (ie, editing existing), append the ID.
          /** @var \Drupal\Core\Entity\EntityForm $form_object */
          $form_object = $form_state->getFormObject();
          if ($form_object instanceof EntityFormInterface && $entity = $form_object->getEntity()) {
            if (!$entity->isNew()) {
              $url->setOption('query', [
                'reader_publication_id' => $entity->id(),
              ]);
            }
          }

          // Add chapter button.
          $label = $this->t('Add Chapter');
          $render = Link::fromTextAndUrl($label, $url)->toRenderable();
          $render['#attributes'] = [
            'class' => [
              'button',
            ],
          ];
          $render['#suffix'] = ' ' . $this->t('Clicking each link here (as well as the add a new chapter button) will open the chapter edit screen, discarding any changes you have made here.');
          $form['field_reader_chapters']['create_chapter'] = $render;
        }
      }
    }
  }

}
