<?php

namespace Drupal\epub_reader_framework\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ReaderChapterFormAlter.
 */
class ReaderChapterFormAlter {

  use StringTranslationTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Callback implementation of hook_form_FORM_ID_alter().
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $form_id
   *   The form ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function formAlter(array &$form, FormStateInterface $form_state, $form_id) {
    if (isset($form['field_reader_components'])) {
      $form['field_reader_components']['#states'] = [
        'visible' => [
          'input[name="field_reader_chapter_automated[value]"]' => ['checked' => FALSE],
        ],
      ];
    }

    if (empty($form['field_reader_publication']['widget'][0]['target_id']['#default_value'])) {
      $target = &$form['field_reader_publication']['widget'][0]['target_id'];

      // Ensure we have a default value.
      if (!$target['#default_value']) {
        $query = \Drupal::request()->query;
        if ($query->has('reader_publication_id')) {
          $reader_publication = $this->entityTypeManager
            ->getStorage('node')
            ->load($query->get('reader_publication_id'));
          $target['#default_value'] = $reader_publication;
        }
      }

      // If we still don't have a default value.
      if (empty($target['#default_value'])) {

        // Send a warning message to the user.
        $message = $this->t('You must first create a Reader Publication. Then upload an EPUB and / or click the "Add chapter" button on your publication edit screen.');
        $this->messenger->addWarning($message);

        // Redirect to adding a publication.
        $url = Url::fromRoute('node.add', [
          'node_type' => 'reader_publication',
        ])->toString();
        $response = new RedirectResponse($url);
        $response->send();
      }
    }

    // Hide the publication selection field.
    if (!empty($form['field_reader_publication']['widget'][0]['target_id'])) {
      hide($form['field_reader_publication']['widget'][0]['target_id']);
    }
  }

}
