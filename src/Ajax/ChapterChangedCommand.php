<?php

namespace Drupal\epub_reader_framework\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class ChapterChangedCommand.
 *
 * @package Drupal\epub_reader_framework\Ajax
 */
class ChapterChangedCommand implements CommandInterface {

  /**
   * The heading text.
   *
   * @var string
   */
  protected $heading = '';

  /**
   * The position of the heading on the page.
   *
   * @var int
   */
  protected $order = 0;

  /**
   * ChapterChangedCommand constructor.
   *
   * @param string $heading
   *   The heading query string parameter.
   * @param int $order
   *   The order query string parameter.
   */
  public function __construct($heading, $order) {
    $this->heading = $heading;
    $this->order = $order;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    return [
      'command' => 'chapterChangedCommand',
      'heading' => $this->heading,
      'order' => $this->order,
    ];
  }

}
