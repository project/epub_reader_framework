<?php

namespace Drupal\epub_reader_framework\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * An ajax command to push an item into the history api state.
 */
class ChapterHistoryPushStateCommand implements CommandInterface {

  /**
   * An array of context data to be added with the url.
   *
   * @var array|null
   */
  protected $state;

  /**
   * The title to associate with the history frame.
   *
   * @var string|null
   */
  protected $title;

  /**
   * The url to be put on the history stack.
   *
   * @var string
   */
  protected $url;

  /**
   * Creates a push history ajax command.
   *
   * @param array|null $state
   *   An array of context data to be added to the history stack or NULL.
   * @param string|null $title
   *   The title to be add to the history stack or NULL.
   * @param string $url
   *   The url to push onto the browser's history stack.
   */
  public function __construct($state, $title, $url) {
    $this->state = $state;
    $this->title = $title;
    $this->url = $url;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'chapterHistoryPushStateCommand',
      'state' => $this->state,
      'title' => $this->title,
      'url' => $this->url,
    ];
  }

}
