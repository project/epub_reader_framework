<?php

namespace Drupal\epub_reader_framework\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\epub_reader_framework\Ajax\ChapterChangedCommand;
use Drupal\epub_reader_framework\Ajax\ChapterHistoryPushStateCommand;
use Drupal\epub_reader_framework\Event\ReaderNodeAjaxControllerResponseEvent;
use Drupal\node\NodeInterface;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class ReaderNodeAjaxController.
 *
 * @package Drupal\epub_reader_framework\Controller
 */
class ReaderNodeAjaxController extends ControllerBase {

  /**
   * Ajax callback to render the reader chapter.
   *
   * Takes different logic paths based on whether Javascript was enabled.
   * If $type == 'ajax', it tells this function that ajax.js has rewritten
   * the URL and thus we are doing an AJAX and can return an array of commands.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to render.
   * @param string $nojs
   *   Either 'ajax' or 'nojs`.
   * @param string|int $back
   *   Either '' or '1`. If 1, indicates, the ajax call is via the back button
   *   and adding to the history again should be avoided.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|\Zend\Diactoros\Response\RedirectResponse
   *   Ajax response if ajax, otherwise redirect.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function nodeAjaxView(NodeInterface $node, $nojs = 'nojs', $back = '') {

    // Determine whether the request is coming from AJAX or not.
    if ($nojs == 'ajax') {
      if ($node->bundle() == 'reader_chapter') {
        $render = \Drupal::entityTypeManager()->getViewBuilder('node')->view($node, 'full');
        $renderer = \Drupal::service('renderer');
        $output = $renderer->render($render);

        // Get query string parameters.
        $heading = \Drupal::request()->get('heading') ?? '';
        $order = \Drupal::request()->get('order') ?? 0;

        // Create the ajax response.
        $response = new AjaxResponse();
        $response->addCommand(new ReplaceCommand('#js-epub-reader-framework-content', $output));

        // If this was not via the back button, add to the ajax history.
        if (!$back) {
          $response->addCommand(new ChapterHistoryPushStateCommand([
            'ajax-url' => '/node/' . $node->id() . '/nojs',
          ],
            $node->label(),
            $node->toUrl()->toString()
          ));
        }
        $response->addCommand(new ChapterChangedCommand($heading, $order));

        // Allow other modules to modify or add to the response.
        $event = new ReaderNodeAjaxControllerResponseEvent($node, $response);

        // Get the event_dispatcher service and dispatch the event.
        $event_name = ReaderNodeAjaxControllerResponseEvent::EVENT_NAME;
        $event_dispatcher = \Drupal::service('event_dispatcher');
        $event_dispatcher->dispatch($event_name, $event);

        // Update the response with the event response in case it changed.
        $response = $event->response;
        return $response;
      }
    }

    // Default to sending the user to the full view.
    return new RedirectResponse($node->toUrl()->toString());
  }

}
