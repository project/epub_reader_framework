<?php

namespace Drupal\epub_reader_framework\Event;

use Drupal\node\NodeInterface;
use Wa72\HtmlPageDom\HtmlPageCrawler;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired when the batch is processing a particular chapter.
 */
class ReaderEpubXhmlConverterEvent extends Event {

  const EVENT_NAME = 'reader_epub_xhtml_converter';

  /**
   * The symfony domcrawler.
   *
   * @var \Wa72\HtmlPageDom\HtmlPageCrawler
   */
  public $crawler;

  /**
   * The reader publication.
   *
   * @var \Drupal\node\NodeInterface
   */
  public $readerPublication;

  /**
   * The reader chapter.
   *
   * @var \Drupal\node\NodeInterface
   */
  public $readerChapter;

  /**
   * Constructs the object.
   *
   * @param \Wa72\HtmlPageDom\HtmlPageCrawler $crawler
   *   The symfony domcrawler.
   * @param \Drupal\node\NodeInterface $reader_publication
   *   The publication.
   * @param \Drupal\node\NodeInterface $reader_chapter
   *   The chapter.
   */
  public function __construct(HtmlPageCrawler $crawler, NodeInterface $reader_publication, NodeInterface $reader_chapter) {
    $this->crawler = $crawler;
    $this->readerPublication = $reader_publication;
    $this->readerChapter = $reader_chapter;
  }

}
