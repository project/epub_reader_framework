<?php

namespace Drupal\epub_reader_framework\Event;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\node\NodeInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired before the ajax response is returned.
 */
class ReaderNodeAjaxControllerResponseEvent extends Event {

  const EVENT_NAME = 'reader_node_ajax_controller_response';

  /**
   * The reader publication or chapter.
   *
   * @var \Drupal\node\NodeInterface
   */
  public $node;

  /**
   * The ajax response.
   *
   * @var \Drupal\Core\Ajax\AjaxResponse
   */
  public $response;

  /**
   * Constructs the object.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The publication or chapter.
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The ajax response.
   */
  public function __construct(NodeInterface $node, AjaxResponse $response) {
    $this->node = $node;
    $this->response = $response;
  }

}
