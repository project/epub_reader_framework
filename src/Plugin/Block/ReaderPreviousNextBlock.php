<?php

namespace Drupal\epub_reader_framework\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\node\NodeInterface;

/**
 * Provides a Reader Previous Next Block.
 *
 * @Block(
 *   id = "reader_previous_next_block",
 *   admin_label = @Translation("Reader previous next block"),
 *   category = @Translation("EPUB Reader"),
 * )
 */
class ReaderPreviousNextBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      \Drupal::moduleHandler()->alter('epub_reader_framework_current_node', $node);

      // Get the reader publication.
      $current_id = FALSE;
      $reader_publication = FALSE;
      if ($node->bundle() == 'reader_publication') {
        $reader_publication = $node;
      }
      elseif ($node->bundle() == 'reader_chapter') {
        $current_id = $node->id();

        /** @var \Drupal\node\NodeInterface $reader_publication */
        $reader_publication = $node->field_reader_publication->entity;
      }

      if ($reader_publication) {
        $previous_id = FALSE;
        $next_id = FALSE;
        $next_entity = FALSE;
        $previous_entity = FALSE;

        // Current item is the publication if not set, therefore already found.
        $found_current_id = ($current_id ? FALSE : TRUE);

        // Build chapters.
        $chapters = $reader_publication->get('field_reader_chapters');
        if ($chapters->count()) {
          foreach ($chapters as $chapter_reference) {
            $id = $chapter_reference->target_id;

            // If we found the current ID but don't yet have the next ID, then
            // this item is the next id.
            if ($found_current_id && !$next_id) {
              $next_id = $id;
              $next_entity = $chapter_reference->entity;
            }

            // If we haven't yet found the current ID and this item isn't the
            // current ID, then store it as the previous.
            if (!$found_current_id && $id != $current_id) {
              $previous_id = $id;
              $previous_entity = $chapter_reference->entity;
            }

            // Store that we have found the current ID.
            if ($id == $current_id) {
              $found_current_id = TRUE;
            }
          }
        }

        $theme = [
          '#theme' => 'reader_previous_next',
          '#previous' => [],
          '#next' => [],
        ];

        // Previous chapter link.
        if ($previous_id) {
          $text = $this->t('Previous chapter');
          $route = 'epub_reader_framework.node_ajax_view';
          $args = [
            'node' => $previous_id,
            'nojs' => 'nojs',
          ];
          $options = [
            'attributes' => [
              'class' => [
                'use-ajax',
                'c-reader-previous-next__link',
                'c-reader-previous-next__link--previous',
                'js-reader-previous-next-link',
                'js-reader-previous-link',
              ],
              'data-title' => [
                ($previous_entity instanceof NodeInterface ? $previous_entity->label() : ''),
              ],
            ],
          ];
          $theme['#previous'] = Link::createFromRoute($text, $route, $args, $options)->toRenderable();
        }

        // Next chapter link.
        if ($next_id) {
          $text = $current_id ? $this->t('Next chapter') : $this->t('Start reading');
          $route = 'epub_reader_framework.node_ajax_view';
          $args = [
            'node' => $next_id,
            'nojs' => 'nojs',
          ];
          $options = [
            'attributes' => [
              'class' => [
                'use-ajax',
                'c-reader-previous-next__link',
                'c-reader-previous-next__link--next',
                'js-reader-previous-next-link',
                'js-reader-next-link',
              ],
              'data-title' => [
                ($next_entity instanceof NodeInterface ? $next_entity->label() : ''),
              ],
            ],
          ];
          $theme['#next'] = Link::createFromRoute($text, $route, $args, $options)->toRenderable();
        }

        return $theme;
      }
    }
    return [];
  }

}
