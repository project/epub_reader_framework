<?php

namespace Drupal\epub_reader_framework;

use Drupal\file\FileInterface;
use Drupal\node\NodeInterface;

/**
 * Class EpubReaderFrameworkHelpers.
 */
class EpubReaderFrameworkHelpers {

  /**
   * Get the EPUB file for a publication.
   *
   * @param \Drupal\node\NodeInterface $reader_publication
   *   The reader publication.
   *
   * @return bool|\Drupal\file\FileInterface
   *   The EPUB file or false.
   */
  public static function getEpubFile(NodeInterface $reader_publication) {
    if ($reader_publication->hasField('field_reader_file') && $media_file = $reader_publication->field_reader_file->entity) {
      /** @var \Drupal\media\MediaInterface $media_file */
      if ($media_file->hasField('field_media_file') && $file = $media_file->field_media_file->entity) {
        /** @var \Drupal\file\FileInterface $file */
        return $file;
      }
    }
    return FALSE;
  }

  /**
   * Get the EPUB file path for a publication.
   *
   * @param \Drupal\node\NodeInterface $reader_publication
   *   The reader publication.
   * @param bool|\Drupal\file\FileInterface $file
   *   The EPUB file or FALSE to attempt to automatically determine it.
   *
   * @return bool|string
   *   The path to the extracted content or false.
   */
  public static function getEpubExtractedFilesPath(NodeInterface $reader_publication, $file = FALSE) {
    if (!$file || !$file instanceof FileInterface) {
      $file = self::getEpubFile($reader_publication);
    }
    if ($file && $file instanceof FileInterface) {

      // Determine storage location.
      /** @var \Drupal\Core\StreamWrapper\StreamWrapperManager $stream_wrapper_manager */
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');
      $stream_wrappers = $stream_wrapper_manager->getWrappers();
      if (isset($stream_wrappers['private']) && $stream_wrappers['private']) {
        $path_uri = 'private://';
      }
      else {
        $path_uri = \Drupal::config('system.file')->get('default_scheme') . '://';
      }

      // Build the path per publication, per file.
      $path_uri .= 'reader-content/publication-' . $reader_publication->id() . '/file-' . $file->id() . '/';

      return $path_uri;
    }

    return FALSE;
  }

}
