<?php

namespace Drupal\epub_reader_framework\Entity;

use Drupal\Core\Entity\EntityInterface;

/**
 * Class ReaderEntityPresave.
 */
class ReaderEntityPresave {

  /**
   * Callback implementation of hook_entity_presave() for reader publication.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved.
   */
  public function entityPresave(EntityInterface $entity) {
    switch ($entity->bundle()) {
      case 'reader_publication':
        $this->batchTriggerCheck($entity);
        break;
    }
  }

  /**
   * Check if we need to trigger a reimport of the publication EPUB.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being saved.
   */
  protected function batchTriggerCheck(EntityInterface $entity) {
    $new_file = FALSE;
    $original_file = FALSE;

    // Get new file.
    if ($entity->hasField('field_reader_file') && $media_file = $entity->field_reader_file->entity) {
      if ($media_file->hasField('field_media_file') && $file = $media_file->field_media_file->entity) {
        $new_file = $file;
      }
    }

    // Get original file.
    if ($entity->original && $entity->original->hasField('field_reader_file') && $media_file = $entity->original->field_reader_file->entity) {
      if ($media_file->hasField('field_media_file') && $file = $media_file->field_media_file->entity) {
        $original_file = $file;
      }
    }

    if ($new_file != $original_file) {

      // If changed, add a flag for the batch to be triggered post save.
      $entity->postsave_batch_start = TRUE;
    }
    elseif ($entity->hasField('field_reader_file_processed') && !$entity->field_reader_file_processed->value) {

      // If something previously went wrong, the file is not yet processed:
      // process it now.
      $entity->postsave_batch_start = TRUE;
    }
  }

}
