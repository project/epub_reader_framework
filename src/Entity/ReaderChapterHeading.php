<?php

namespace Drupal\epub_reader_framework\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the Reader Chapter Heading entity.
 *
 * @ingroup epub_reader_framework
 *
 * @ContentEntityType(
 *   id = "reader_chapter_heading",
 *   label = @Translation("Reader Chapter Heading"),
 *   base_table = "reader_chapter_heading",
 *   entity_keys = {
 *     "id" = "id",
 *     "reader_publication_id" = "reader_publication_id",
 *     "reader_chapter_id" = "reader_chapter_id",
 *   },
 * )
 */
class ReaderChapterHeading extends ContentEntityBase implements ContentEntityInterface {

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Reader Chapter Heading entity.'))
      ->setReadOnly(TRUE);

    $fields['reader_publication_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Reader Publication ID'))
      ->setDescription(t('The ID of the Reader Publication entity.'));

    $fields['reader_chapter_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Reader Chapter ID'))
      ->setDescription(t('The ID of the Reader Chapter entity.'));

    $fields['order'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Order'))
      ->setDescription(t('The order of the chapter headings.'));

    $fields['heading'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Heading'))
      ->setDescription(t('The heading within the chapter.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ]);

    return $fields;
  }

}
