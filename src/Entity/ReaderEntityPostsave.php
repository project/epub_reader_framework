<?php

namespace Drupal\epub_reader_framework\Entity;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\epub_reader_framework\Batch\ReaderChapterExtractorBatchProcessor;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ReaderEntityPostsave.
 */
class ReaderEntityPostsave {

  use StringTranslationTrait;

  /**
   * The render entity cross reference service.
   *
   * @var \Drupal\epub_reader_framework\Entity\ReaderEntityCrossReference
   */
  protected $readerEntityCrossReference;

  /**
   * The render entity cross reference service.
   *
   * @var \Drupal\epub_reader_framework\Entity\ReaderEntityChapterHeadingsSave
   */
  protected $readerEntityChapterHeadingsSave;

  /**
   * Constructor.
   *
   * @param \Drupal\epub_reader_framework\Entity\ReaderEntityCrossReference $reader_entity_cross_reference
   *   The reader entity cross reference service.
   * @param \Drupal\epub_reader_framework\Entity\ReaderEntityChapterHeadingsSave $reader_entity_chapter_headings_save
   *   The reader entity chapter headings save service.
   */
  public function __construct(
    ReaderEntityCrossReference $reader_entity_cross_reference,
    ReaderEntityChapterHeadingsSave $reader_entity_chapter_headings_save
  ) {
    $this->readerEntityCrossReference = $reader_entity_cross_reference;
    $this->readerEntityChapterHeadingsSave = $reader_entity_chapter_headings_save;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('epub_reader_framework.reader_entity_cross_reference'),
      $container->get('epub_reader_framework.reader_entity_chapter_headings_save')
    );
  }

  /**
   * Callback implementation of insert and update entity hooks.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being inserted or updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function entityPostsave(EntityInterface $entity) {

    // Ensure this does not run recursively.
    $route_name = \Drupal::routeMatch()->getRouteName();

    // Only when this particular node type was being edited.
    switch ($entity->bundle()) {
      case 'reader_publication':
        // Potentially batch process for import if editing or adding a
        // publication; happens if EPUB has changed.
        if (in_array($route_name, ['entity.node.edit_form', 'node.add'])) {
          $this->batchTrigger($entity);
        }
        break;

      case 'reader_chapter':
        $this->chapterCrossReference($entity);
        $this->chapterHeadingsSave($entity);

        // Redirect to the publication if editing or adding a chapter.
        if (in_array($route_name, ['entity.node.edit_form', 'node.add'])) {
          $this->redirectToPublication($entity);
        }
        break;
    }
  }

  /**
   * Maybe trigger the batch.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being inserted or updated.
   */
  protected function batchTrigger(EntityInterface $entity) {

    // Check for flag set in pre-save.
    if (isset($entity->postsave_batch_start) && $entity->postsave_batch_start) {
      $entity->postsave_batch_start = FALSE;

      $operation_callback = [
        ReaderChapterExtractorBatchProcessor::class,
        'operationCallback',
      ];
      $finish_callback = [
        ReaderChapterExtractorBatchProcessor::class,
        'finishedCallback',
      ];
      $batch_builder = (new BatchBuilder())
        ->setTitle($this->t('Extracting chapters from the EPUB'))
        ->setFinishCallback($finish_callback)
        ->setInitMessage($this->t('Chapter extraction is starting'))
        ->setProgressMessage($this->t('Currently extracting chapters.'))
        ->setErrorMessage($this->t('Chapter extraction has encountered an error.'));

      $batch_builder->addOperation($operation_callback, [$entity->id()]);
      batch_set($batch_builder->toArray());
    }
  }

  /**
   * Maybe add a chapter cross-reference.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being inserted or updated.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function chapterCrossReference(EntityInterface $entity) {
    /** @var \Drupal\node\NodeInterface $entity */
    /** @var \Drupal\node\NodeInterface $reader_publication */
    $reader_publication = $entity->field_reader_publication->entity;

    /** @var \Drupal\epub_reader_framework\Entity\ReaderEntityCrossReference $reader_entity_cross_reference */
    $reader_entity_cross_reference = $this->readerEntityCrossReference;
    $reader_entity_cross_reference->ensureChapterIsCrossReferencedInPublication(
      $reader_publication,
      $entity
    );
  }

  /**
   * Save chapter headings.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being inserted or updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function chapterHeadingsSave(EntityInterface $entity) {
    /** @var \Drupal\node\NodeInterface $entity */
    /** @var \Drupal\node\NodeInterface $reader_publication */
    $reader_publication = $entity->field_reader_publication->entity;

    /** @var \Drupal\epub_reader_framework\Entity\ReaderEntityChapterHeadingsSave $reader_entity_chapter_headings_save */
    $reader_entity_chapter_headings_save = $this->readerEntityChapterHeadingsSave;
    $reader_entity_chapter_headings_save->saveHeadings(
      $reader_publication,
      $entity
    );
  }

  /**
   * Redirect to the publication.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being inserted or updated.
   */
  protected function redirectToPublication(EntityInterface $entity) {
    /** @var \Drupal\node\NodeInterface $entity */
    /** @var \Drupal\node\NodeInterface $reader_publication */
    $reader_publication_id = $entity->field_reader_publication->target_id;

    // Redirect to adding a publication.
    $url = Url::fromRoute('entity.node.edit_form', [
      'node' => $reader_publication_id,
    ])->toString();
    $response = new RedirectResponse($url);
    $response->send();
  }

}
