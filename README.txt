README.txt for EPUB Reader Framework module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------

About the EPUB Reader

This module is intended as a base framework for creating a digital version of a
publication from an EPUB (eg, exported from InDesign).

What this module does

  1. Creates two node types: 'Reader Publication' and 'Reader Chapter'
  2. Upload the EPUB file into a new 'Reader publication' and save
  3. A batch process runs to automatically extract the EPUB and use the
     'Spine' of the EPUB create (or update) Reader Chapters by EPUB Chapter ID
  4. Allows you to subsequently re-order, modify, and add additional chapters
     (eg, adding a video where the print publication would skip such rich media

Where this module needs to be extended

  1. The module extracts the individual chapter xhtml files and during the
     batch import provides an instance of HtmlPageCrawler as well as the Reader
     Chapter being processed as an Event
  2. Subscribe to the Event (see the example module) and decide how you want to
     display it on your site. You could extract all contents as is to a WYSIWYG
     body field. You could extract contents into Paragraphs to create a more
     component-based approach.


REQUIREMENTS
------------

If not installed via composer, the following dependencies must be loaded:

  1. symfony/dom-crawler
  2. symfony/css-selector
  3. wa72/htmlpagedom


INSTALLATION
------------

 - Install the EPUB Reader Framework module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


MAINTAINERS
-----------

 - Scott Euser (scott_euser) https://www.drupal.org/u/scott_euser

 - Supporting organizations
   Soapbox Communications Ltd
