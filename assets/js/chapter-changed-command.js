(function ($, Drupal) {

  /**
   * Add new command for responding to the chapter change.
   */
  Drupal.AjaxCommands.prototype.chapterChangedCommand = function (ajax, response, status) {
    $('#js-epub-reader-framework-content').imagesLoaded(function () {

      // Find a particular heading within the page if exists.
      var scroll_target = '#js-epub-reader-framework-content';
      var offset = 0;
      if (response.heading) {
        var heading_text = response.heading.replace(/(['"])/g, "\\$1");
        if (heading_text.length > 0) {
          if ($('#js-epub-reader-framework-content h2:contains(' + heading_text + ')').length) {
            scroll_target = '#js-epub-reader-framework-content h2:contains(' + heading_text + ')';
            offset += 16;
          }
        }
      }
      if (!offset && response.order) {
        var order = parseInt(response.order);
        if (order > 0) {
          if ($('#js-epub-reader-framework-content h2:nth-child(' + response.order + ')').length) {
            scroll_target = '#js-epub-reader-framework-content h2:nth-child(' + response.order + ')';
            offset += 16;
          }
        }
      }

      // Compensate for toolbar position.
      if ($('body').hasClass('toolbar-fixed')) {
        offset += $('#toolbar-bar').height();
      }
      if ($('body').hasClass('toolbar-tray-open')) {
        offset += $('.toolbar-tray').first().height();
      }

      // Compensate for any sort of sticky nav.
      var sticky_element = $('.js-reader-sticky-nav');
      if (sticky_element.length) {
        offset += sticky_element.height();
      }

      // Scroll to the ajax loaded content.
      $('body').scrollTo(scroll_target, 1000, {
        offset: (-1 * offset)
      });
    });
  }
})(jQuery, Drupal);
