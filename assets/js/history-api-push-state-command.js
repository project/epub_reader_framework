(function ($, Drupal) {

  /**
   * Add new command for responding adding the chapter change to the history.
   */
  Drupal.AjaxCommands.prototype.chapterHistoryPushStateCommand = function (ajax, response, status) {
    window.history.pushState(response.state, response.title, response.url);
  }
})(jQuery, Drupal);
