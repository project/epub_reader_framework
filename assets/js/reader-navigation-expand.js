(function ($, Drupal) {

  "use strict";

  /**
   * Reader expand collapse.
   */
  Drupal.behaviors.readerNavigationExpand = {
    attach: function (context) {

      // Toggle show or hide headings within each chapter.
      function processNavigationExpandOrCollapse(self) {

        // Ensure this is only called once.
        if (context === document) {
          self.closest('.js-reader-navigation-expand-wrapper', context).closest('li').find('ul:first').slideToggle('fast', function () {
            self.closest('.js-reader-navigation-expand-wrapper', context).toggleClass('is-expanded');
          });
        }
      }

      // Handle click on the icon.
      $(context).on('click', '.js-reader-navigation-expand', function() {
        processNavigationExpandOrCollapse($(this));
      });

      // Handle enter keypress on the icon.
      $(context).on('keypress', '.js-reader-navigation-expand', function(event){
        if(event.which === 13) {
          processNavigationExpandOrCollapse($(this));
        }
      });
    }
  };
})(jQuery, Drupal);
