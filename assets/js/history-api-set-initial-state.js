(function ($, Drupal) {

  "use strict";

  var initial_state_set = false;

  /**
   * Reader history api set initial state.
   *
   * Event.state will be empty unless we set initial state.
   *
   * @see https://stackoverflow.com/a/11844412
   */
  Drupal.behaviors.historyApiSetInitialState = {
    attach: function (context) {
      if (!initial_state_set) {
        var relative_url = window.location.pathname + window.location.search;
        if (window.location.hash) {
          relative_url += window.location.hash;
        }
        var title = $(document).find('title').text();
        var data = [];
        data['ajax-url'] = relative_url;

        // Replace the current history state adding in the ajax-url.
        window.history.replaceState(data, title, relative_url);
        initial_state_set = true;
      }
    }
  };
})(jQuery, Drupal);
