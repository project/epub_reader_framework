(function ($, Drupal) {

  "use strict";

  /**
   * Reader react to history api state change.
   */
  Drupal.behaviors.readerHistory = {
    attach: function (context) {
      window.addEventListener('popstate', function(event) {
        if (event.state) {
          var url = event.state['ajax-url'];

          // Ensure this is a relative internal path.
          if (typeof url === 'string' && url.substring(0, 1) === '/') {
            if (url.indexOf('/nojs') > 0) {

              // Ajax call.
              Drupal.ajax({
                url: url.replace('/nojs', '/ajax') + '/back',
              }).execute();
            }
            else {

              // Normal redirect.
              window.location.replace(url);
            }
          }
        }
      });
    }
  };
})(jQuery, Drupal);
